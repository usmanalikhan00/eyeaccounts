import { Component } from '@angular/core';
import * as PouchDB from 'pouchdb'
import { Event, Router, NavigationEnd, NavigationStart } from  '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  loading = true;
  // public userDB = new PouchDB('users');
  constructor(private _router: Router) {
	  this._router.events.subscribe((routerEvent: Event) => {
	    if (routerEvent instanceof NavigationStart){
	      console.log("NAVIGATION START");
	      this.loading = false
	    }
	    if (routerEvent instanceof NavigationEnd){
	      console.log("NAVIGATION END");
	      this.loading = true
	    }
	  });
	}
}
