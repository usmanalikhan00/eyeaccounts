import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
PouchDB.setMaxListeners(50)
import * as moment from "moment";

@Injectable()
export class SalaryService {
  
  public salaryDB = new PouchDB('aajeelaccsalaries');

  constructor(private _router: Router){
  }

  addSalary(values){
    var self = this;
    return self.salaryDB.put(values);
  }

  allSalaries(){
    var self = this;
    return self.salaryDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }


}