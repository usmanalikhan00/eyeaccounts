import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class InvoiceReceiptService {
  
  authenticatedUser: any = [];
  public invoicesReceiptsDB = new PouchDB('aajeelaccinvoicereceipts');
  

  constructor(private _router: Router){}

  addInvoiceReceipt(invoiceReceipt){
    var self = this;
    console.log("INVOICE RECEIPT DOCUMENT TO PUT:-----", invoiceReceipt)
    return self.invoicesReceiptsDB.put(invoiceReceipt);
  }

  allInvocieReceipts(){
    var self = this;
    return self.invoicesReceiptsDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSingleInvoiceReceipt(receiptId){
    var self = this;
    return self.invoicesReceiptsDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.invoicesReceiptsDB.find({
        selector: {_id:{$eq: receiptId}}
      });
    })
  }

  // stockItemDetails(product){
  //   // PouchDB.debug.enable('pouchdb:find')
  //   var self = this;
  //   return self.invoicesReceiptsDB.createIndex({
  //     index: {
  //       fields: ['invoiceproducts']
  //     }
  //   }).then(function(){
  //     return self.invoicesReceiptsDB.find({
  //       selector: {
  //         invoiceproducts:{
  //           $elemMatch:{_id:{$eq:product._id}}
  //         }
  //       }
  //     });
  //   })
  // }

  getRefAccountPayments(accountrefid){
    var self = this;
    return self.invoicesReceiptsDB.createIndex({
      index: {
        fields: ['accountrefid']
      }
    }).then(function(){
      return self.invoicesReceiptsDB.find({
        selector: {accountrefid: {$eq: accountrefid}}
      });
    })
  }

}