import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
PouchDB.setMaxListeners(50)
import * as moment from "moment";

@Injectable()
export class EmployeeService {
  
  public employeesDB = new PouchDB('aajeelaccemployees');

  constructor(private _router: Router){
  }

  addEmployee(values){
    var self = this;
    return self.employeesDB.put(values);
  }

  allEmployees(){
    var self = this;
    return self.employeesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }


}