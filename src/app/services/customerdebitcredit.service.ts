import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class CustomerDebitCreditService {
  
  authenticatedUser: any = [];
  public customerDebitCreditDB = new PouchDB('aajeelacccustomerdebitcredit');
  

  constructor(private _router: Router){}

  addDebitCredit(customer){
    console.log("CUSTOMER FOR DEBIT CREDIT:-----", customer)
    var self = this;
    return self.customerDebitCreditDB.put(customer)
  }

  allDebitCredit(){
    var self = this;
    return self.customerDebitCreditDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }


  updateDebitCredit(debitcredits){
    console.log("CUSTOMER DEBIT CREDITs:-----", debitcredits)
    var self = this;
    return self.customerDebitCreditDB.bulkDocs(debitcredits)
  }


  
  getDebitCredits(customer){
    var self = this;
    return self.customerDebitCreditDB.createIndex({
      index: {
        fields: ['customerid']
      }
    }).then(function(){
      return self.customerDebitCreditDB.find({
        selector: {
          customerid:{
            $eq:customer._id
          }
        }
      });
    })
  }

  getInvoiceDebitCredits(){
    var self = this;
    return self.customerDebitCreditDB.createIndex({
      index: {
        fields: ['dctype']
      }
    }).then(function(){
      return self.customerDebitCreditDB.find({
        selector: {
          dctype:{
            $eq:"invoice"
          }
        }
      });
    })
  }

  getPurchaseDebitCredits(){
    var self = this;
    return self.customerDebitCreditDB.createIndex({
      index: {
        fields: ['dctype']
      }
    }).then(function(){
      return self.customerDebitCreditDB.find({
        selector: {
          dctype:{
            $eq:"purchase"
          }
        }
      });
    })
  }



  getPaymentDebitCredits(){
    var self = this;
    return self.customerDebitCreditDB.createIndex({
      index: {
        fields: ['dctype']
      }
    }).then(function(){
      return self.customerDebitCreditDB.find({
        selector: {
          dctype:{
            $eq:"payment"
          }
        }
      });
    })
  }

  getReceiptDebitCredits(){
    var self = this;
    return self.customerDebitCreditDB.createIndex({
      index: {
        fields: ['dctype']
      }
    }).then(function(){
      return self.customerDebitCreditDB.find({
        selector: {
          dctype:{
            $eq:"receipt"
          }
        }
      });
    })
  }







  printDebitCredits(customer){
    var self = this;
    return self.customerDebitCreditDB.createIndex({
      index: {
        fields: ['_id', 'customerid']
      }
    }).then(function(){
      return self.customerDebitCreditDB.find({
        selector: { 
          $and: [
            {
              customerid:{ 
                $eq: customer._id
              }
            },
            {
              _id:{
                $exists:true
              }
            },
            {
              _id:{
                $gt:null
              }
            }
          ]
        },
        sort: [{_id:'asc'}]
      })
    })
  }

  filterDebitCredits(customer, toDate, fromDate){
    var self = this;
    console.log(fromDate, toDate)
    return self.customerDebitCreditDB.createIndex({
      index: {
        fields: ['_id', 'customerid']
      }
    }).then(function(){
      return self.customerDebitCreditDB.find({
        selector: { 
          $and: [
            {
              customerid:{ 
                $eq: customer._id
              }
            },
            {
              _id:{
                $exists:true
              }
            },
            {
              _id:{
                $lte: toDate
              }
            },
            {
              _id:{
                $gte: fromDate
              }
            }

          ]
        },
        sort: [{_id:'asc'}]
      })
    })
  }

  backDatedDebitCredit(customer, date){
    var self = this;
    console.log("backdated debit credit ________::", date)
    return self.customerDebitCreditDB.createIndex({
      index: {
        fields: ['_id', 'customerid']
      }
    }).then(function(){
      return self.customerDebitCreditDB.find({
        selector: { 
          $and: [
            {
              customerid:{ 
                $eq: customer._id
              }
            },
            {
              _id:{
                $exists:true
              }
            },
            {
              _id:{
                $lte: date
              }
            }
          ]
        },
        sort: [{_id:'desc'}],
        limit: 1
      })
    })


  }

}