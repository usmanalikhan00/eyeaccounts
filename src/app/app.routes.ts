import { Routes, RouterModule } from '@angular/router';
import { Login, 
         deodashboard,
         userdashboard,
         admindashboard,
         products,
         addproduct,
         adminsettings,
         productcategories,
         subcategories,
         addInvoice,
         invoice,
         customers,
         cashsettings,
         subAccountType,
         addcustomer,
         storesettings,
         singleinvoice,
         purchase,
         addpurchase,
         singlepurchase,
         inwardpass,
         addinwardpass,
         singleinwardpass,
         outwardpass,
         addoutwardpass,
         singleoutwardpass,
         convertoutward,
         convertinward,
         inventoryledger,
         customerledger,
         accountsledger,
         purchasepayment,
         invoicereceipt,
         invoicereturn,
         purchasereturn,
         printsingleinvoice,
         printinwardpass,
         printoutwardpass,
         printpurchase,
         printpayment,
         singleinvoicereturn,
         printinvoicereturn,
         printpurchasereturn,
         singlepurchasereturn,
         printpurchases,
         printinvoices,
         printproducts,
         printcustomers,
         printreceipt,
         cashtransfers,
         Employees,
         addEmployee,
         Salaries,
         addSalary,
         companyinfo,
         Headers,
         ToolBar,
         SiteLayout,
         TestComponent } from './components/component-index';
import { AuthGuard } from './_gaurds/auth.gaurd'



export const ROUTES: Routes = [
  

  { path: '',      
    component: Login 
  },
  {
    path: 'salaries',
    component: Salaries,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printproducts',
    component: printproducts,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'addproduct',
    component: addproduct,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'inventoryledger/:productId',
    component: inventoryledger,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'subaccounts/:accountId',
    component: subAccountType,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'accountsledger/:accountId',
    component: accountsledger,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printinvoices',
    component: printinvoices,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'invoice/:invoiceId',
    component: singleinvoice,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printsingleinvoice/:invoiceId',
    component: printsingleinvoice,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printreceipt/:receiptId',
    component: printreceipt,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'singleinvoicereturn/:invoicereturnId',
    component: singleinvoicereturn,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printinvoicereturn/:invoicereturnId',
    component: printinvoicereturn,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printpurchases',
    component: printpurchases,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printpayment/:paymentId',
    component: printpayment,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'purchase/:purchaseId',
    component: singlepurchase,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printpurchase/:purchaseId',
    component: printpurchase,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printpayment/:paymentId',
    component: printpayment,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'singlepurchasereturn/:purchasereturnId',
    component: singlepurchasereturn,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printpurchasereturn/:purchasereturnId',
    component: printpurchasereturn,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'inwardpass/:inwardpassId',
    component: singleinwardpass,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printinwardpass/:inwardpassId',
    component: printinwardpass,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printoutwardpass/:outwardpassId',
    component: printoutwardpass,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'outwardpass/:outwardpassId',
    component: singleoutwardpass,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'printcustomers',
    component: printcustomers,
    canActivate: [ AuthGuard ]
  },
  
  {
    path: 'addcustomer',
    component: addcustomer,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'customerledger/:customerId',
    component: customerledger,
    canActivate: [ AuthGuard ]
  },
  { path: 'login',  
    component: Login 
  },
  { path: 'employees',  
    component: Employees, 
    canActivate: [ AuthGuard ]
  },
  
  // {
  //   path: 'salaries',
  //   component: Salaries,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'deodashboard',
  //   component: deodashboard,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'userdashboard',
  //   component: userdashboard,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'admindashboard',
  //   component: admindashboard,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'products',
  //   component: products,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'companyinfo',
  //   component: companyinfo,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'invoice',
  //   component: invoice,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'addinvoice',
  //   component: addInvoice,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'invoicereceipt',
  //   component: invoicereceipt,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'invoicereturn',
  //   component: invoicereturn,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'purchase',
  //   component: purchase,
  //   canActivate: [ AuthGuard ]
  // },  
  // {
  //   path: 'addpurchase',
  //   component: addpurchase,
  //   canActivate: [ AuthGuard ]
  // },  
  // {
  //   path: 'purchasepayment',
  //   component: purchasepayment,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'purchasereturn',
  //   component: purchasereturn,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'inwardpass',
  //   component: inwardpass,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'addinwardpass',
  //   component: addinwardpass,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'convertinward/:inwardpassId',
  //   component: convertinward,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'outwardpass',
  //   component: outwardpass,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'addoutwardpass',
  //   component: addoutwardpass,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'convertoutward/:outwardpassId',
  //   component: convertoutward,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'customers',
  //   component: customers,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'cashsettings',
  //   component: cashsettings,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'storesettings',
  //   component: storesettings,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'cashtransfers',
  //   component: cashtransfers,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'adminsettings',
  //   component: adminsettings,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'productcategories',
  //   component: productcategories,
  //   canActivate: [ AuthGuard ]
  // },
  // {
  //   path: 'subcategories/:categoryId',
  //   component: subcategories,
  //   canActivate: [ AuthGuard ]
  // },

  { 
    path: '', 
    component: SiteLayout,
    children: [
      // DASHBOARDS 
      { path: 'admindashboard', 
        component: admindashboard, 
        canActivate: [AuthGuard]
      },
      {
        path: 'deodashboard',
        component: deodashboard,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'userdashboard',
        component: userdashboard,
        canActivate: [ AuthGuard ]
      },
      //  ADMIN SETTINGS
      {
        path: 'adminsettings',
        component: adminsettings,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'storesettings',
        component: storesettings,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'cashsettings',
        component: cashsettings,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'companyinfo',
        component: companyinfo,
        canActivate: [ AuthGuard ]
      },
      // PRODUCTS
      { path: 'products', 
        component: products, 
        canActivate: [AuthGuard]
      },
      {
        path: 'productcategories',
        component: productcategories,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'subcategories/:categoryId',
        component: subcategories,
        canActivate: [ AuthGuard ]
      },
      // INVOICES
      {
        path: 'invoice',
        component: invoice,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'addinvoice',
        component: addInvoice,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'invoicereturn',
        component: invoicereturn,
        canActivate: [ AuthGuard ]
      },
      // PURCHASES  
      {
        path: 'purchase',
        component: purchase,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'addpurchase',
        component: addpurchase,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'purchasereturn',
        component: purchasereturn,
        canActivate: [ AuthGuard ]
      }, 
      // CUSTOMERS
      {
        path: 'customers',
        component: customers,
        canActivate: [ AuthGuard ]
      },
      // CASH TRANSFER
      {
        path: 'cashtransfers',
        component: cashtransfers,
        canActivate: [ AuthGuard ]
      },
      // INWARD PASS  
      {
        path: 'inwardpass',
        component: inwardpass,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'addinwardpass',
        component: addinwardpass,
        canActivate: [ AuthGuard ]
      },
      // OUTWARD PASS
      {
        path: 'outwardpass',
        component: outwardpass,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'addoutwardpass',
        component: addoutwardpass,
        canActivate: [ AuthGuard ]
      },
      // INVOICE RECEIPT
      {
        path: 'invoicereceipt',
        component: invoicereceipt,
        canActivate: [ AuthGuard ]
      },
      // PURCHASE PAYMENT
      {
        path: 'purchasepayment',
        component: purchasepayment,
        canActivate: [ AuthGuard ]
      },
      // CONVERT INWARD OUTWARD
      {
        path: 'convertinward/:inwardpassId',
        component: convertinward,
        canActivate: [ AuthGuard ]
      },
      {
        path: 'convertoutward/:outwardpassId',
        component: convertoutward,
        canActivate: [ AuthGuard ]
      },
    ]
  }

];
