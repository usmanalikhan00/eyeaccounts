import * as _ from "lodash";
import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: "capFilter"
})
export class CapFilterPipe implements PipeTransform {
  transform(query: string): any {
    if (query){
      if(query.indexOf(' ') !== -1){
          var inputPieces, i;

          query = query.toLowerCase();
          inputPieces = query.split(' ');

          for(i = 0; i < inputPieces.length; i++){
              inputPieces[i] = capitalizeString(inputPieces[i]);
          }

          return inputPieces.toString().replace(/,/g, ' ');
      }else{
        query = query.toLowerCase();
        return capitalizeString(query);
      }

    }
    function capitalizeString(inputString){
      return inputString.substring(0,1).toUpperCase() + inputString.substring(1);
    }
  }
}