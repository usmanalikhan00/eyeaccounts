import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { PurchaseService } from '../../../services/purchase.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { InvoiceReturnService } from '../../../services/invoicereturn.service'
import { InwardPassService } from '../../../services/inwardpass.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";

import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";
import { ElectronService } from 'ngx-electron'

@Component({
    selector: 'single-invoicereturn',
    providers: [LoginService, 
                ProductService, 
                InvoiceService, 
                PurchaseService, 
                ElectronService, 
                InwardPassService, 
                InvoiceReturnService],
    templateUrl: 'singleinvoicereturn.html',
    styleUrls: ['singleinvoicereturn.css']
})

export class singleinvoicereturn {
  
  // public productSubCategoriesDB = new PouchDB('productsubcategories');
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  // addSubCategoryForm: FormGroup;
  // allSubCategories: any = []
  sub: any;
  invoicereturnId: any;
  selectedInvoiceReturn: any;
  purchaseNotes: any;
  authUser: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _inwardPassService: InwardPassService, 
              private _invoiceReturnService: InvoiceReturnService, 
              private _electronService: ElectronService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }




  ngOnInit(){
    var self = this;

    self.sub = self._activatedRouter.params.subscribe(params => {
      self.invoicereturnId = params['invoicereturnId']; // (+) converts string 'id' to a number
      console.log("INWARDID TO GET DETAIL:-----", self.invoicereturnId)
      self._invoiceReturnService.getSingleInvoiceReturn(self.invoicereturnId).then(function(result){
        // console.log("RESULT FROM SINLE CATEGORY", result)
        self.selectedInvoiceReturn = result.docs[0]
        console.log("RESULT FROM SELECTED INWARD PASS:---------", self.selectedInvoiceReturn)
      }).catch(function(err){
          console.log(err)
      })

    });
  }


  goToPrint(){
    // this._router.navigate(['printinvoicereturn', this.selectedInvoiceReturn._id])
    this._electronService.ipcRenderer.send('InvoiceReturnPrint', this.selectedInvoiceReturn._id)
  }


  goBack(){
    this._router.navigate(['purchase'])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }


  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
