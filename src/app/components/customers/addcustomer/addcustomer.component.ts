import { Component, ElementRef, ViewChild, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { CustomersService } from '../../../services/customers.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {DataSource} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material';
import {BehaviorSubject, Observable} from 'rxjs';



import { MatSidenav } from "@angular/material";

@Component({
    selector: 'add-customer',
    providers: [LoginService, 
                ProductService,
                CustomersService],
    templateUrl: 'addcustomer.html',
    styleUrls: ['addcustomer.css']
})

export class addcustomer {
  
  public customersDB = new PouchDB('steelcustomers');
  @ViewChild('sidenav') sidenav: MatSidenav;
  @Output() addcustomer = new EventEmitter();
  navMode = 'side';
  addCustomerForm: FormGroup;
  allCustomers: any = [];
  balanceTypes = ['Credit', 'Debit']

  constructor(private _loginService: LoginService,
              private _productService: ProductService,
              private _customersService: CustomersService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddCustomerForm();
  }

  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      fax: [''],
      openingbalance:['', Validators.required],
      obtype:['', Validators.required]
    })
  }

  ngOnInit(){
    var self = this;
  }

  addCustomer(values){
    var self = this;
    if (values.obtype === 'Debit'){
      if (values.openingbalance != 0){
        values.closingbalance = values.openingbalance
        values.status = 'debit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }else if (values.obtype === 'Credit'){
      if (values.openingbalance != 0){
        values.openingbalance = -values.openingbalance
        values.closingbalance = values.openingbalance
        values.status = 'credit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }
    console.log("ADD CUSTOMER CALLED:--", values);
    self._customersService.addCustomer(values).then(function(result){
      console.log("Customer ADDED:===", result);
      self.addCustomerForm.reset();
      self.addcustomer.emit({'success': 'done'})
        // self.addProductForm.clearValidators();
      // console.log("CHANGE VARIACLE", changes);
    }).catch(function(err){
      console.log(err);
    })
  }

  goBack(){
    this._router.navigate(['customers'])
  }

  // @HostListener('window:resize', ['$event'])
  //   onResize(event) {
  //       if (event.target.innerWidth < 886) {
  //           this.navMode = 'over';
  //           this.sidenav.close();
  //       }
  //       if (event.target.innerWidth > 886) {
  //          this.navMode = 'side';
  //          this.sidenav.open();
  //       }
  //   }

  logout(){
    this._loginService.logout()
  }  
}