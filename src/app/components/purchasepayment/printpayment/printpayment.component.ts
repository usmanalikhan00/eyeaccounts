import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { PurchaseService } from '../../../services/purchase.service'
import { PurchasePaymentService } from '../../../services/purchasepayment.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";

import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";
import {ElectronService} from 'ngx-electron';
require('events').EventEmitter.prototype._maxListeners = 100;

@Component({
    selector: 'print-payment',
    providers: [LoginService, 
                ProductService, 
                InvoiceService, 
                ElectronService, 
                PurchaseService, 
                PurchasePaymentService],
    templateUrl: 'printpayment.html',
    styleUrls: ['printpayment.css']
})

export class printpayment {
  

  sub: any;
  paymentId: any;
  selectedPayment: any;
  invoiceNotes: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _purchaseService: PurchaseService, 
              private _purchasePaymentService: PurchasePaymentService, 
              private _electronService: ElectronService, 
              private _router: Router) {
  }

  ngOnInit(){
    var self = this;
    self.sub = self._activatedRouter.params.subscribe(params => {
      self.paymentId = params['paymentId']; // (+) converts string 'id' to a number
      console.log("PARAMETER SINGLE PAYMENT ID:-----", self.paymentId)
      self._purchasePaymentService.getSinglePurchasePayment(self.paymentId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
        self.selectedPayment = result.docs[0]
        console.log("RESULT FROM SINLE Payment", self.selectedPayment)
        self._electronService.ipcRenderer.send('PrintPayment')
      }).catch(function(err){
          console.log(err)
      })
    });
  }

  goBack(){
    this._router.navigate(['invoice'])
  }


  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
