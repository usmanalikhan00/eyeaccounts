import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { PurchaseService } from '../../../services/purchase.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { InwardPassService } from '../../../services/inwardpass.service'
import { OutwardPassService } from '../../../services/outwardpass.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
import { ElectronService } from "ngx-electron";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";

import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'single-outwardpass',
    providers: [LoginService, 
                ProductService, 
                InvoiceService, 
                PurchaseService,
                OutwardPassService, 
                ElectronService, 
                InwardPassService],
    templateUrl: 'singleoutwardpass.html',
    styleUrls: ['singleoutwardpass.css']
})

export class singleoutwardpass {
  

  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  sub: any;
  outwardpassId: any;
  selectedOutwardpass: any;
  purchaseNotes: any;
  authUser: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _inwardPassService: InwardPassService, 
              private _formBuilder: FormBuilder,
              private _outwardPassService: OutwardPassService,
              private _electronService: ElectronService,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  ngOnInit(){
    var self = this;

    self.sub = self._activatedRouter.params.subscribe(params => {
      self.outwardpassId = params['outwardpassId']; // (+) converts string 'id' to a number
      console.log("INWARDID TO GET DETAIL:-----", self.outwardpassId)
      self.getSingleOutwardPass()

    });
  }
  
  getSingleOutwardPass(){
    var self = this;
    self._outwardPassService.getSingleOutwardpass(self.outwardpassId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
      self.selectedOutwardpass = result.docs[0]
      console.log("RESULT FROM SELECTED OUT WARD PASS:---------", self.selectedOutwardpass)
    }).catch(function(err){
        console.log(err)
    })

  }

  goToPrint(){
    // this._router.navigate(['printoutwardpass', this.selectedOutwardpass._id])
    this._electronService.ipcRenderer.send('OutwardpassPrint', this.selectedOutwardpass._id)
  }

  goBack(){
    this._router.navigate(['purchase'])
  }

  convertToInvoice(){
      console.log("CONVERTED TO INVOICE CALLECD ON:---------", this.selectedOutwardpass._id)
    this._router.navigate(['/convertoutward', this.selectedOutwardpass._id])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
