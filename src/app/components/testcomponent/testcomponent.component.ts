import {Component, ElementRef} from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
    selector: 'test-component',
    providers: [FormBuilder],
    templateUrl: 'testcomponent.html',
    styleUrls: ['testcomponent.css']
})

export class TestComponent {
  
  loggedUser: any = null;
  
  constructor(private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("TEST COMPOENT STARTED:--", this.loggedUser)
  }

}