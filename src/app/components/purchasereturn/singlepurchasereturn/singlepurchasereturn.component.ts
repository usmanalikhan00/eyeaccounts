import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { PurchaseService } from '../../../services/purchase.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { PurchaseReturnService } from '../../../services/purchasereturn.service'
import { InwardPassService } from '../../../services/inwardpass.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";

import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";
import { ElectronService } from "ngx-electron";

@Component({
    selector: 'single-purchasereturn',
    providers: [LoginService, 
                ProductService, 
                InvoiceService, 
                PurchaseService, 
                ElectronService, 
                InwardPassService, 
                PurchaseReturnService],
    templateUrl: 'singlepurchasereturn.html',
    styleUrls: ['singlepurchasereturn.css']
})

export class singlepurchasereturn {
  
  // public productSubCategoriesDB = new PouchDB('productsubcategories');
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  // addSubCategoryForm: FormGroup;
  // allSubCategories: any = []
  sub: any;
  purchasereturnId: any;
  selectedPurchaseReturn: any;
  purchaseNotes: any;
  authUser: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _inwardPassService: InwardPassService, 
              private _purchaseReturnService: PurchaseReturnService, 
              private _formBuilder: FormBuilder,
              private _electronService: ElectronService,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }




  ngOnInit(){
    var self = this;

    self.sub = self._activatedRouter.params.subscribe(params => {
      self.purchasereturnId = params['purchasereturnId']; // (+) converts string 'id' to a number
      console.log("PurchaseD TO GET DETAIL:-----", self.purchasereturnId)
      self._purchaseReturnService.getSinglePurchaseReturn(self.purchasereturnId).then(function(result){
        // console.log("RESULT FROM SINLE CATEGORY", result)
        self.selectedPurchaseReturn = result.docs[0]
        console.log("RESULT FROM SELECTED PurchasePASS:---------", self.selectedPurchaseReturn)
      }).catch(function(err){
          console.log(err)
      })

    });
  }


  goToPrint(){
    // this._router.navigate(['printpurchasereturn', this.selectedPurchaseReturn._id])
    this._electronService.ipcRenderer.send('PurchaseReturnPrint', this.selectedPurchaseReturn._id)
  }


  goBack(){
    this._router.navigate(['purchase'])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }


  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
