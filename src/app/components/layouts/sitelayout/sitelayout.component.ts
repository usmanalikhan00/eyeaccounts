import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { Event, Router, NavigationEnd, NavigationStart } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {LoginService} from '../../../services/login.service'
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'site-layout',
    providers: [FormBuilder, LoginService],
    templateUrl: 'sitelayout.html',
    styleUrls: ['sitelayout.css']
})

export class SiteLayout {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode: any = 'side';
  authUser: any = null;
  showLoadingSpinner: any = false; 


  color = 'warn';
  mode = 'indeterminate';
  value = 50;
  bufferValue = 75;

  constructor(private _formBuilder: FormBuilder, 
              private _loginService: LoginService,
              private _router: Router) {
    this._router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart){
        console.log("NAVIGATION START");
        this.showLoadingSpinner = true
      }
      if (routerEvent instanceof NavigationEnd){
        console.log("NAVIGATION END");
        this.showLoadingSpinner = false
      }
    });
  }

  
  ngOnInit(){
    var self = this;
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
  
  logout(){
    this._loginService.logout()
  }

}