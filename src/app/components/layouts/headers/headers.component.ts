import {Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from  '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'app-header',
    providers: [FormBuilder],
    templateUrl: 'headers.html',
    styleUrls: ['headers.css']
})

export class Headers {
  
  loggedUser: any = null;
  @ViewChild('sidenav') sidenav: MatSidenav;
    
  constructor(private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("APP HEADER STARTED:--", this.loggedUser)
  }

}