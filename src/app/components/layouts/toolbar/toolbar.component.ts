import {Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from  '@angular/router';
// import { SchoolService } from  '../../../services/schools.services';
// import { InboxService } from  '../../../services/inbox.service';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'tool-bar',
    providers: [FormBuilder],
    templateUrl: 'toolbar.html',
    styleUrls: ['toolbar.css']
})

export class ToolBar {

  loggedUser: any = null;
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
    
  constructor(private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    this.loggedUser = JSON.parse(localStorage.getItem('user'))
    console.log("APP HEADER STARTED:--", this.loggedUser)
  }

}